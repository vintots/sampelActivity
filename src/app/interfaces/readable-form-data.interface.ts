export interface IReadableFormData {
    fullName: string;
    category: string;
    product: string;
    order: string;
}
