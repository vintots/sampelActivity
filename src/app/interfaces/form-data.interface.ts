export interface IFormData {
    fullName: string;
    categoryId: number;
    productId: number;
    orderId: number;
}
