export interface IStoreData {
    categories: Array<ICategory>;
    products: Array<IProduct>;
    orders: Array<IOrder>;
}

export interface ICategory {
    id: number;
    description: string;
}

export interface IProduct {
    categoryId: number;
    id: number;
    description: string;
}

export interface IOrder {
    productId: number;
    id: number;
    description: string;
}
