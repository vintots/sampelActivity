import { MainPageComponent } from './main-page/main-page.component';
import { FormPageComponent } from './form-page/form-page.component';
import { ConfirmationPageComponent } from './confirmation-page/confirmation-page.component';

export const PageDeclaration = [
    MainPageComponent,
    FormPageComponent,
    ConfirmationPageComponent
];
