import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/index';
import { IReadableFormData } from '../../interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmation-page',
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.scss']
})
export class ConfirmationPageComponent implements OnInit {
  formData: IReadableFormData;
  constructor(
    private router: Router,
    private storeService: StoreService
  ) { }

  ngOnInit() {
    if (this.storeService.formData) {
      this.formData = this.storeService.formatFormData(this.storeService.formData);
    } else {
      this.router.navigate(['/form']);
    }

  }

}
