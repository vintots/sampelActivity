


import { MainPageComponent } from './main-page/main-page.component';
import { FormPageComponent } from './form-page/form-page.component';
import { ConfirmationPageComponent } from './confirmation-page/confirmation-page.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


const routes: Routes = [
    {
        path: '',
        component: MainPageComponent
    },
    {
        path: 'form',
        component: FormPageComponent
    },
    {
        path: 'confirmation',
        component: ConfirmationPageComponent
    },
    { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PageRoutingModule { }
