import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page.component';
import { PageRoutingModule } from './page-routing.module';
import { PageDeclaration } from './index';
import { SharedServices } from '../services/shared.services';
import { MaterialCollection } from '../shared/material';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PageRoutingModule,
    HttpClientModule,
    ...MaterialCollection
  ],
  declarations: [
    PageComponent,
    ...PageDeclaration
  ],
  providers: [...SharedServices]
})
export class PageModule { }
