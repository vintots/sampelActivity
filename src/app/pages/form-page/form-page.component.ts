import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBuilderService, ApiService, StoreService } from '../../services/index';
import { IStoreData, IProduct, IOrder } from '../../interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-page',
  templateUrl: './form-page.component.html',
  styleUrls: ['./form-page.component.scss']
})
export class FormPageComponent implements OnInit {
  entryForm: FormGroup;
  public storeData: IStoreData;
  public products: Array<IProduct> = [];
  public orders: Array<IOrder> = [];
  constructor(
    private fb: FormBuilderService,
    private apiService: ApiService,
    private storeService: StoreService,
    private routes: Router
  ) { }

  ngOnInit() {
    this.entryForm = this.fb.initForm();
    this.apiService.fetchStoreData();

    this.storeService.behavioralStoreData.subscribe((data: IStoreData) => this.storeData = data);
  }

  /******************************************************************
   * Filterd data and assign to ist corresponding property
   * @param key string
   * @param filterKey string
   * @param id number
   * @returns void
   *****************************************************************/
  filterData(key: string, filterKey: string, id: number): void {
    const filteredData = this.storeService.filterData(key, filterKey, id);
    switch (key) {
      case 'products':
        this.products = filteredData as Array<IProduct>;
        this.entryForm.get('orderId').setValue('');
        break;
      case 'orders':
        this.orders = filteredData as Array<IOrder>;
        break;
      default:
        this.entryForm.get('productId').setValue('');
        this.entryForm.get('orderId').setValue('');
        break;
    }
  }

  submit(): void {
    this.storeService.formData = this.entryForm.value;
    this.routes.navigate(['confirmation']);
  }

}
