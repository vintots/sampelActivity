import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ROUTES_CONFIG } from './constants/routes-config.constant';

const routes: Routes = [
    {
        path: 'app',
        component: AppComponent,
        children: [
            {
                path: `${ROUTES_CONFIG.MAIN_PAGE}`,
                loadChildren: './pages/page.module#PageModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class ActivityRoutingModule { }
