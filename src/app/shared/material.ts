/***********Materialize Imports  ***************************/
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';

export const MaterialCollection = [
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatButtonModule
];
