import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_CONFIG } from '../constants/api-config.const';
import { IStoreData } from '../interfaces/index';
import { StoreService } from './store.service';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class ApiService {
    private _subscription: Subscription;
    constructor(
        private http: HttpClient,
        private storeService: StoreService
    ) { }

    fetchStoreData(): void {
        // Check if the storeDAta is null before fetching data
        if (!this.storeService.hasFetched) {
            this._subscription = this.http.get<IStoreData>(API_CONFIG.storeData)
                .subscribe((data: IStoreData) => this.storeService.storeData = data);
        }

    }
}
