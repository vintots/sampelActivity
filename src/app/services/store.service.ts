import { Injectable } from '@angular/core';
import { IStoreData, ICategory, IProduct, IOrder, IReadableFormData } from '../interfaces/index';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IFormData } from '../interfaces/form-data.interface';

@Injectable()
export class StoreService {
    private _storeData = new BehaviorSubject(null);
    private _rawData: IStoreData;
    public hasFetched = false;
    private _formData: IFormData;
    constructor() { }

    get behavioralStoreData() {
        return this._storeData;
    }

    set storeData(data: IStoreData) {
        this._storeData.next(data);
        this._rawData = data;
        this.hasFetched = true;
    }

    get formData() {
        return this._formData;
    }

    set formData(data: IFormData) {
        this._formData = data;
    }

    /************************************************
     * Filter Store data via key and param id
     * @param key string
     * @param id number
     * @returns  Array<ICategory> | Array<IProduct> | Array<IOrder>
     *********************************************/
    filterData(key: string, filterKey: string, id: number): Array<ICategory> | Array<IProduct> | Array<IOrder> {
        if (this._rawData[key]) {
            const data = this._rawData[key].filter((item: ICategory | IProduct | IOrder) => item[filterKey] === id);
            return data.length ? data : this._rawData[key];
        }
        return null;
    }

    formatFormData(data: IFormData): IReadableFormData {
        return {
            fullName: data.fullName,
            category: this.getDescriptionByKeyAndId('categories', data.categoryId),
            product: this.getDescriptionByKeyAndId('products', data.productId),
            order: this.getDescriptionByKeyAndId('orders', data.orderId),
        };
    }

    getDescriptionByKeyAndId(key: string, id: number): string {
        if (this._rawData[key]) {
            const data = this._rawData[key].find((item: ICategory | IProduct | IOrder) => item.id === id);
            return data ? data.description : null;
        }

    }
}
