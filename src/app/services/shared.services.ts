import { FormBuilderService } from './form-builder.service';
import { StoreService } from './store.service';
import { ApiService } from './api.service';

export const SharedServices = [
    FormBuilderService,
    StoreService,
    ApiService
];
