import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class FormBuilderService {

    constructor(
        private fb: FormBuilder
    ) { }

    /***************************************
     * Creation of Form
     * Initialize empty values
     **************************************/
    initForm(): FormGroup {
        return this.fb.group({
            fullName: ['', [Validators.required, Validators.pattern('([a-zA-Z.]\\s*)+')]],
            categoryId: ['', Validators.required],
            productId: ['', Validators.required],
            orderId: ['', Validators.required],
        });

    }

}
